const dgram = require('dgram');
const server = dgram.createSocket('udp4');

const multicast = '239.255.255.250';
const multicastPort = 1982;

const message = Buffer.from(`M-SEARCH * HTTP/1.1\r\nHOST: ${multicast}:${multicastPort}\r\nMAN: "ssdp:discover"\r\nST: wifi_bulb`);

server.on('error', (err) => server.close());

server.on('message', (msg, { address, port }) => {
    console.log({ address, port })
    console.log(msg.toString('utf8'))
});

server.on('listening', () => server.send(message, multicastPort, multicast));

server.bind(multicastPort);


