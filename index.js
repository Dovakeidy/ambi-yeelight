const http = require('http');
const net = require('net');

const tvIP = '192.168.1.22';
const tvPort = 1925;

const yeelightIP = '192.168.1.10';
const yeelightPort = 55443;

const serverIP = '192.168.1.25';
const severPort = 3000;

const refreshRate = 50;
const smoothDuration = 50;

net.createServer((client) => {
    console.log('client connected');
  
    client.on('end', () =>  console.log('client disconnected'));

    client.on('error', () =>  console.log('error'));

    setInterval(() => {
        const req = http.request({
            hostname: tvIP,
            port: tvPort,
            path: '/6/ambilight/measured',
            method: 'GET'
        }, res => {
            res.on('data', data => {
                const { r, g, b } = JSON.parse(data.toString('utf8'))['layer1']['top'][2];
    
                const bits = parseInt(((+r)*65536) + ((+g)*256) + (+b), 10);
    
                console.log(bits)
    
                client.write(Buffer.from(`{"id":1,"method":"set_rgb","params":[${bits}, "smooth", ${smoothDuration}]}\r\n`));
            });
        });

        req.end();
    }, refreshRate);
}).listen(severPort, () => console.log('server bound'));

(() => {
    const client = new net.Socket();

    client.connect(yeelightPort, yeelightIP, () => {
        client.write(Buffer.from(`{"id":1,"method":"set_music","params":[1, "${serverIP}", ${severPort}]}\r\n`));
    });

    client.on('data', (e) => console.log(e.toString('utf-8')))
})();

